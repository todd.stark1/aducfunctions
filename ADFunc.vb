﻿'This file is part of ADUCFunc.dll.

'ADUCFunc.dll is free software: you can redistribute it and/or modify
'it under the terms of the GNU Lesser Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.

'ADUCFunc.dll is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
'GNU Lesser Public License for more details.

'You should have received a copy of the GNU Lesser Public License
'along with ADUCFunc.dll.  If not, see <http://www.gnu.org/licenses/>.
Imports System.DirectoryServices.ActiveDirectory
Imports System.DirectoryServices

Public Class ADFunc
    Public Shared Function GetADForest() As String
        'Returns current Active Directory Forest for domain as string
        Dim result As String = ""
        Try
            result = Forest.GetCurrentForest.ToString
        Catch ex As Exception
            result = "UNKNOWN"
        End Try
        Return result
    End Function 'GetADForest()
    Public Shared Function GetADDomain() As String
        'Returns current Active Directory domain as string
        Dim result As String = ""
        Try
            result = Domain.GetCurrentDomain.ToString
        Catch ex As Exception
            result = "UNKNOWN"
        End Try
        Return result
    End Function 'GetADDomain()
    Public Shared Function GetADSite() As String
        'Returns Active Directory site name that you are currently connected to.
        Dim result As String = Nothing
        Try
            result = ActiveDirectorySite.GetComputerSite().ToString
        Catch ex As Exception
            result = "UNKNOWN"
        End Try
        Return result
    End Function 'GetADSite()
    Public Shared Function GetLogonServer() As String
        'Returns current LogonServer/DC as string removing leading "\\"
        'I would like to find a way to do this with dotnet instead of using system variables
        Dim result As String
        Try
            result = Replace(My.Application.GetEnvironmentVariable("LOGONSERVER"), "\\", "")
        Catch ex As Exception
            result = "UNKOWN"
        End Try
        Return result
    End Function 'GetLogonServer()
    Public Shared Function GetCurrentUser() As String
        'Returns currently logged in user as string in "DOMAIN\user" format
        Dim result As String = Nothing
        Try
            result = My.User.Name
        Catch ex As Exception
            result = "UNKNOWN"
        End Try
        Return result
    End Function 'GetCurrentUser()
    Public Shared Function DirSearch(SearchPath As String, Filter As String, Properties As Array) As SearchResultCollection
        Dim rootEntry As New DirectoryEntry(SearchPath)
        Dim searcher As New DirectorySearcher(rootEntry)
        Dim queryResults As SearchResultCollection = Nothing

        searcher.PropertiesToLoad.AddRange(Properties)
        searcher.Filter = Filter

        queryResults = searcher.FindAll()

        Return queryResults
    End Function
    Public Shared Sub MoveObject(ADObject As DirectoryEntry, Destination As DirectoryEntry)

        Try
            ADObject.MoveTo(Destination)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub 'MoveObject
End Class 'ADFunc
